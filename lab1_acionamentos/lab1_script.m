%laboratorio 1-a ACIONAMENTOS ELETRONICOS DE MOTORES
%1
n = 0:1:1800;

%tensao terminal em volts
Vt = 120;
%resistencia da aramdura em ohms
Ra = 0.2;
%resistencia de campo em ohms
Rf = 40;
%constante da maquina em Nm/A
Kaf = 0.876; 

Te = (30*Vt*Kaf-pi*(Kaf^2)*n) /(30*Ra);

Tc = 15 + (2e-5)*(n.^2);
%figure(1);
%plot(n, Te, n, Tc);

%2
%aumentando a tensao para 80 v
Vt1 = 80;
Te = (30*Vt1*Kaf-pi*(Kaf^2)*n) /(30*Ra);
plot(n, Te, n, Tc);

%3
%constante alterado para 0.8
Kaf1 = 0.8; 
Te = (30*Vt*Kaf1-pi*(Kaf1^2)*n) /(30*Ra);
plot(n, Te, n, Tc);

%4
Rext = 0.42-Ra;
Te = (30*Vt*Kaf-pi*(Kaf^2)*n) /(30*Ra);
Te1 = (30*Vt*Kaf-pi*(Kaf^2)*n) /(30*(Ra+Rext));
plot(n, Te, n, Te1, n, Tc);